package ar.utn.dds;

public interface Consultoria {

	public Boolean estaCercaDe() ;
	
	public Boolean estaDisponible();
	
	public void buscarPalabraClave(); 

	
}
