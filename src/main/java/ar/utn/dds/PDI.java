package ar.utn.dds;

import org.uqbar.geodds.Point;

import ar.utn.dds.exceptions.NombreVacioExceptions;
import ar.utn.dds.exceptions.UbicacionVacioExceptions;

public class PDI
{	

	private String nombre;
	private Point ubicacion;
	private	int cant;
	public PDI() {
	 
	}
	
	public String getNombrePDI() {
		return nombre;
	}
	
	public void setNombrePDI(String nombre)throws NombreVacioExceptions {
		if (nombre==null || nombre.equals("")){
			throw new NombreVacioExceptions("el nombre no puede estar vacio");
			}
		this.nombre = nombre;
	}
	public void setCantNombrePDI(int cant,String nombre){
		this.cant = nombre.length();	}
	public int getCantNombrePDI(){
		return cant; 
	}
	public Point getUbicacionPDI() {
		return ubicacion;
	}
	
	public void setUbicacionPDI(Point ubicacion)throws UbicacionVacioExceptions{
		if (ubicacion==null){
			throw new UbicacionVacioExceptions("la ubicacion no puede estar vacio");
		}
		this.ubicacion = ubicacion;
	}
	 public boolean equals(Object obj) {
		 if (this == obj)
		 return true;
		 else
		return false;
		 
	 }



}
