package ar.utn.dds;

public class Direccion 
{
	private String callePrincipal;
	private String entreCalles;
	private int numeroDeDireccion;
	private DatosEspecificos datosEspecificos;	

	public Direccion(){
		
	}

	public String getCallePrincipal() {
		return callePrincipal;
	}

	public void setCallePrincipal(String callePrincipal) {
		this.callePrincipal = callePrincipal;
	}

	public String getEntreCalles() {
		return entreCalles;
	}

	public void setEntreCalles(String entreCalles) {
		this.entreCalles = entreCalles;
	}

	public int getNumeroDeDireccion() {
		return numeroDeDireccion;
	}

	public void setNumeroDeDireccion(int numeroDeDireccion) {
		this.numeroDeDireccion = numeroDeDireccion;
	}

	public DatosEspecificos getDatosEspecificos() {
		return datosEspecificos;
	}

	public void setDatosEspecificos(DatosEspecificos datosEspecificos) {
		this.datosEspecificos = datosEspecificos;
	}
}