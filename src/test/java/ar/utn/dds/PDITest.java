package ar.utn.dds;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.uqbar.geodds.Point;

import ar.utn.dds.exceptions.NombreVacioExceptions;
import ar.utn.dds.exceptions.UbicacionVacioExceptions;


public class PDITest {
	private PDI pdiAProbar;
	private String unNombre = "Garbarino";
 
	private Point unLugar = new Point(1, 2) ;
		
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}	
	
	@Before 
	public void setUp()throws NombreVacioExceptions, UbicacionVacioExceptions{
		
		this.pdiAProbar = new PDI();
		this.pdiAProbar.setNombrePDI(this.unNombre);
		this.pdiAProbar.setCantNombrePDI(this.unNombre);
		this.pdiAProbar.setUbicacionPDI(this.unLugar);

			}
	
	
	@Test
	public void testEquals() throws NombreVacioExceptions, UbicacionVacioExceptions{
		PDI pdiIgual = new PDI();
		pdiIgual.setNombrePDI(this.unNombre);
		pdiIgual.setCantNombrePDI(unNombre);
		pdiIgual.setUbicacionPDI(this.unLugar);		
		assert(this.equals(pdiIgual));
		}
	
	
	@Test
	public void testconDiferenciaDeNLetras() throws NombreVacioExceptions, UbicacionVacioExceptions {
		PDI pdiConMenosLetrasEnELNombre = new PDI();
		
		pdiConMenosLetrasEnELNombre.setNombrePDI("Garnarin");
		
		String unNombre2 = pdiConMenosLetrasEnELNombre.getNombrePDI();
		
		pdiConMenosLetrasEnELNombre.setCantNombrePDI(unNombre2);
		
		int cantidad1 = pdiConMenosLetrasEnELNombre.getCantNombrePDI();			
		
		pdiConMenosLetrasEnELNombre.setUbicacionPDI(new Point(1, 2));
		
		assertTrue(cantidad1<pdiAProbar.getCantNombrePDI());
	}
	@Test
	
	public void testConNombreVacio() throws NombreVacioExceptions, UbicacionVacioExceptions{
		PDI pdiSinNombre=new PDI();
			pdiSinNombre.setUbicacionPDI(new Point(4, 9));
	}
	
	@Test
	
	public void TestConUbicacionVacio() throws NombreVacioExceptions, UbicacionVacioExceptions{
		PDI pdiSinUbicacion=new PDI();
			pdiSinUbicacion.setNombrePDI("Fravega");
			pdiSinUbicacion.setUbicacionPDI(new Point(5, 9));
			Point unaUbic = pdiSinUbicacion.getUbicacionPDI();
			assertFalse(unaUbic==null);
	}
	
	@Test
	
	public void testNull()throws NombreVacioExceptions, UbicacionVacioExceptions{
		PDI pdiNull = new PDI();
		assert(this.equals(pdiNull));

	}
}

