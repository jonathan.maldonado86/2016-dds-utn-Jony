package ar.utn.dds.exceptions;

public class NombreVacioExceptions extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NombreVacioExceptions(String unMensaje) {
		super(unMensaje);
	}

}
