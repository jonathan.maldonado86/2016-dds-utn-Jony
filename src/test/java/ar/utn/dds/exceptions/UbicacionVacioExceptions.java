package ar.utn.dds.exceptions;

public class UbicacionVacioExceptions extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UbicacionVacioExceptions(String unMensaje) {
		super(unMensaje);
	}

}
